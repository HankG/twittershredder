# Twitter Shredder

A Dart based command line tool for deleting all of your Twitter data.
This is most useful for when you want to keep your account but delete
the data in it. There are sites that do this but I didn't feel
comfortable giving them access to my account to perform this operation.

## Features

This tool allows you to delete all of the content of your Twitter account 
that is accessible via the Twitter API. This can be done by deleting an 
individual Tweet or Like. It also can do bulk deletes of all people you follow,
likes, and statuses (Tweets, Re-tweets, and Replies). The source of these
values can either be the Twitter API itself or an archive you dumped and 
unzipped. This latter feature is important because not all tweets and likes
that are in the system show up in the Twitter API's commands [see this post on the topic](https://nequalsonelifestyle.com/2022/00/07/twitters-very-inconsistent-data-consistency/).

## Installation

First be sure that the Dart runtime is installed: https://dart.dev/get-dart

Next, check out the source code and import dependencies:

```bash
git clone https://gitlab.com/HankG/twittershredder.git
cd twittershredder
dart pub get
```

## Running 
To run you need to have [Twitter API credentials](https://developer.twitter.com/en/docs/twitter-api). 
If you have an existing Twitter App whose credentials you want to use feel free to use that.
If you need to create them then [follow these instructions](https://developer.twitter.com/en/docs/authentication/oauth-1-0a/api-key-and-secret).

You need to store your Twitter App's credentials in a JSON file that is accessible from
the command line tool. The file format looks like:

```json
{
    "consumerKey": "",
    "consumerSecret": "",
    "tokenData": {
        "token": "",
        "secret": "",
        "userId": "",
        "screenName": ""
    } 
}
```

You run the command by using the Dart command line tool and providing the 
path to the credentials file and then arguments for which operations you wish. 
Executing it without any arguments will bring up the command line help which reads as:

```bash
Error with arguments: Option credentials is mandatory.
--credentials (mandatory)    Twitter credentials file for accessing the API
--bulk                       Bulk delete option
                             [all, following, likes, statuses]
--use-archive                Use a specified Twitter archive folder path for bulk Status/Like delete source (otherwise uses API)
--historydb-path             Specify a history location
                             (defaults to "./history.db")
--delete-tweet               Delete specific tweet
--delete-like                Delete specific like
```

### Examples

Delete a specific tweet

```bash
dart run bin/twitter_shredder.dart --credentials ~/twitter_credentials.json --delete-tweet 1234567890
```

Delete a like tweet

```bash
dart run bin/twitter_shredder.dart --credentials ~/twitter_credentials.json --delete-like 1234567890
```

Delete all the statuses that the API can see:

```bash
dart run bin/twitter_shredder.dart --credentials ~/twitter_credentials.json --bulk statuses
```

Delete all the likes that an archive has listed:

```bash
dart run bin/twitter_shredder.dart --credentials ~/twitter_credentials.json --bulk likes --use-archive ~/twitter_archive
```