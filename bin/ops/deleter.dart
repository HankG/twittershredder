import 'dart:io';

import 'package:http/http.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';

import '../history/history_interface.dart';
import '../streams/top_id_stream_interface.dart';
import '../types/deleter_error.dart';
import '../types/expiring_cache.dart';

const _maxIntValue = 2147483647; //2,147,483,648
const defaultNormalSleep = Duration(seconds: 1);
const defaultRateLimitSleep = Duration(minutes: 1);
const defaultLongSleep = Duration(minutes: 5);
final _logger = Logger('Deleter');

class Deleter<T> {
  final ITopIDStream idStream;
  final IHistory history;
  final Future<T> Function(String id) deleteFunction;
  final Function(T item) statusFunction;
  final ExpiringCache<String> rateLimitCache;
  final String unitType;

  Deleter({
    required this.idStream,
    required this.history,
    required this.rateLimitCache,
    required this.deleteFunction,
    required this.statusFunction,
    required this.unitType,
  });

  FutureResult<T, DeleterError> deleteSpecific(String id) async {
    final storageId = '${id}_$unitType';
    rateLimitCache.purge();
    if (rateLimitCache.isFull) {
      return Result.error(DeleterError(type: DeleterErrorType.rateLimit));
    }

    final historyResult = history.getItem(storageId);
    if (historyResult.isSuccess) {
      _logger.info('Already operated on this ID: $historyResult');
      return Result.error(DeleterError(
          type: DeleterErrorType.alreadyExists, message: storageId));
    }

    try {
      final item = await deleteFunction(id);
      history.addItem(storageId, '200');
      statusFunction(item);
      return Result.ok(item);
    } on Response catch (r) {
      final msg = '$unitType ID $id was not found or error, skipped deleting';
      _logger.info(msg);
      history.addItem(storageId, r.statusCode.toString());
      return Result.error(
          DeleterError(type: DeleterErrorType.apiError, message: msg));
    } finally {
      rateLimitCache.addItem(storageId);
    }
  }

  FutureResult<int, DeleterError> bulkDelete({
    int max = _maxIntValue,
    Duration normalSleep = defaultNormalSleep,
    Duration rateLimitSleep = defaultRateLimitSleep,
    Duration longSleep = defaultLongSleep,
  }) async {
    int count = 0;
    await for (final id in idStream.ids()) {
      var result = await deleteSpecific(id);
      bool needRetry =
          result.isFailure && result.error.type == DeleterErrorType.rateLimit;
      bool skipSleep = result.isFailure &&
          result.error.type == DeleterErrorType.alreadyExists;
      bool firstPass = true;

      while (needRetry) {
        final sleepTime = firstPass ? rateLimitSleep : longSleep;
        _logger.info('Rate limited, sleeping for $sleepTime');
        sleep(sleepTime);
        result = await deleteSpecific(id);
        needRetry =
            result.isFailure && result.error.type == DeleterErrorType.rateLimit;
        firstPass = false;
      }

      if (result.isSuccess) {
        count++;
      }

      if (count >= max) {
        break;
      }

      if (!skipSleep) {
        sleep(normalSleep);
      }
    }
    return Result.ok(count);
  }
}
