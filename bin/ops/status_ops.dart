import 'package:dart_twitter_api/twitter_api.dart';
import 'package:logging/logging.dart';

import '../utils/utils.dart';

void deleteStatusOutput(Tweet tweet) {
  Logger.root
      .info('Deleted tweet ${tweet.idStr}: ${tweet.fullText?.truncate()}');
}

Future<Tweet> deleteStatus(TwitterApi twitterApi, String id) async =>
    await twitterApi.tweetService.destroy(id: id);
