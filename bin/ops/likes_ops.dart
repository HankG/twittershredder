import 'package:dart_twitter_api/twitter_api.dart';
import 'package:logging/logging.dart';

import '../utils/utils.dart';

class LikeDeleter {
  final bool withForce;

  LikeDeleter({this.withForce = false});

  void deleteLikeOutput(Tweet tweet) {
    Logger.root
        .info('Deleted like for ${tweet.idStr}: ${tweet.fullText?.truncate()}');
  }

  Future<Tweet> deleteLike(TwitterApi twitterApi, String id) async {
    try {
      await twitterApi.tweetService.createFavorite(id: id);
    } catch (_) {
      // sometimes tweets in an archive are phantom so need to be "reliked"
      // but if they were already liked or don't exist anymore than so be it.
      // it is the delete that matters
    }
    return await twitterApi.tweetService.destroyFavorite(id: id);
  }
}
