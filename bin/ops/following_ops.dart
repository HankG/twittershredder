import 'package:dart_twitter_api/twitter_api.dart';
import 'package:logging/logging.dart';

void deleteFollowingOutput(User user) {
  Logger.root.info('No longer following ${user.screenName}');
}

Future<User> deleteFollowingUser(TwitterApi twitterApi, String id) async =>
    twitterApi.userService.friendshipsDestroy(userId: id);
