import 'package:dart_twitter_api/twitter_api.dart';

import '../history/history_interface.dart';
import '../streams/top_id_stream_interface.dart';
import '../types/expiring_cache.dart';
import 'deleter.dart';
import 'following_ops.dart';
import 'likes_ops.dart';
import 'status_ops.dart';

class DeleterFactory {
  final defaultMaxItemsPerInterval = 50;
  final defaultInterval = Duration(minutes: 15);

  final TwitterApi twitterApi;
  final IHistory history;

  DeleterFactory(this.twitterApi, this.history);

  Deleter<User> buildFollowingDeleter(ITopIDStream idStream) => Deleter<User>(
      idStream: idStream,
      history: history,
      rateLimitCache:
          ExpiringCache<String>(defaultMaxItemsPerInterval, defaultInterval),
      deleteFunction: (id) async => await deleteFollowingUser(twitterApi, id),
      statusFunction: (user) => deleteFollowingOutput(user),
      unitType: 'Following');

  Deleter<Tweet> buildLikeDeleter(ITopIDStream idStream, bool withForce) {
    final likeDeleter = LikeDeleter(withForce: withForce);
    return Deleter<Tweet>(
        idStream: idStream,
        history: history,
        rateLimitCache:
            ExpiringCache<String>(defaultMaxItemsPerInterval, defaultInterval),
        deleteFunction: (id) async =>
            await likeDeleter.deleteLike(twitterApi, id),
        statusFunction: (like) => likeDeleter.deleteLikeOutput(like),
        unitType: 'Like');
  }

  Deleter<Tweet> buildStatusDeleter(ITopIDStream idStream) => Deleter<Tweet>(
      idStream: idStream,
      history: history,
      rateLimitCache:
          ExpiringCache<String>(defaultMaxItemsPerInterval, defaultInterval),
      deleteFunction: (id) async => await deleteStatus(twitterApi, id),
      statusFunction: (status) => deleteStatusOutput(status),
      unitType: 'Tweet');
}
