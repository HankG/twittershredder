import 'package:result_monad/result_monad.dart';
import 'package:sqlite3/sqlite3.dart';

import 'history_interface.dart';

class ShredderHistory implements IHistory {
  final String historyDbPath;
  final Database _db;

  ShredderHistory(this.historyDbPath) : _db = sqlite3.open(historyDbPath) {
    try {
      _db.execute('''
      CREATE TABLE history (
        id TEXT NOT NULL PRIMARY KEY,
        timestamp INTEGER NOT NULL,
        status TEXT NOT NULL
      );
    ''');
    } on Exception catch (_) {
      // do nothing, the table already exists so didn't need to create it
    }
  }

  @override
  Result<HistoryItem, String> addItem(String id, String status) {
    final item = HistoryItem(id: id, timestamp: DateTime.now(), status: status);
    try {
      final stmt = _db.prepare(
          'INSERT INTO history (id, timestamp, status) VALUES (?,?,?)');
      stmt.execute([id, DateTime.now().millisecondsSinceEpoch, status]);
      stmt.dispose();
    } on Exception catch (e) {
      return Result.error(e.toString());
    }
    return Result.ok(item);
  }

  @override
  Result<HistoryItem, String> getItem(String id) {
    final ResultSet resultSet =
        _db.select('SELECT * FROM history WHERE id = ?', [id]);
    if (resultSet.isEmpty) {
      return Result.error('ID $id not found');
    }

    final row = resultSet.first;
    final idFromRow = row['id'];
    final timestampMS = row['timestamp'];
    final timestamp = DateTime.fromMillisecondsSinceEpoch(timestampMS);
    final status = row['status'];
    final item =
        HistoryItem(id: idFromRow, timestamp: timestamp, status: status);
    return Result.ok(item);
  }

  void dispose() {
    _db.dispose();
  }
}

class HistoryItem {
  final String id;
  final DateTime timestamp;
  final String status;

  HistoryItem(
      {required this.id, required this.timestamp, required this.status});

  @override
  String toString() {
    return 'HistoryItem{id: $id, timestamp: $timestamp, status: $status}';
  }
}
