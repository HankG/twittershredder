import 'dart:async';
import 'dart:io';

import 'package:args/args.dart';
import 'package:dart_twitter_api/twitter_api.dart';
import 'package:http/http.dart';
import 'package:logging/logging.dart';

import 'history/shredder_history.dart';
import 'ops/deleter_factory.dart';
import 'streams/empty_id_stream.dart';
import 'streams/twitter_archive_id_stream.dart';
import 'streams/twitter_client_id_stream.dart';
import 'twitter/twitter_archive_reader.dart';
import 'twitter/twitter_credentials.dart';
import 'types/bulk_types.dart';
import 'utils/app_args.dart';
import 'utils/logging.dart';

Future<void> main(List<String> args) async {
  await setupLogging();
  final logger = Logger.root;
  final argsParser = buildArgs();
  late ArgResults settings;
  try {
    settings = argsParser.parse(args);
  } on ArgParserException catch (e) {
    print("Error with arguments: ${e.message}");
    print(argsParser.usage);
    return;
  }

  final credentialsResult = fromTSettingsJsonFile(settings[credentialsKeyword]);
  if (credentialsResult.isFailure) {
    logger.severe('Error getting credentials: ${credentialsResult.error}');
    return;
  }

  final twitterApi = TwitterApi(client: credentialsResult.value);
  final history =
      ShredderHistory(settings[historyLocation] ?? defaultHistoryPath);
  final factory = DeleterFactory(twitterApi, history);
  try {
    TwitterArchiveReader? reader;
    if (settings[useArchiveKeyword] != null) {
      final archivePath = settings[useArchiveKeyword];
      if (!Directory(archivePath).existsSync()) {
        logger.info(
            'Requested archive path $archivePath is not accessible, quitting');
        return;
      }
      reader = TwitterArchiveReader(archivePath);
    }

    final bulkType = BulkTypes.parse(settings[bulkKeyword] ?? '');
    if (bulkType != null) {
      final fromArchive = reader != null;
      if (bulkType == BulkTypes.all || bulkType == BulkTypes.likes) {
        logger.info(
            'Attempting to delete all likes according to ${fromArchive ? "archive" : "Twitter API"}');
        final stream = fromArchive
            ? TwitterArchiveIDStream.fromArchiveLikes(reader)
            : TwitterClientIDStream(
                twitterApi, TwitterClientIDStreamType.likes);
        final deleter =
            factory.buildLikeDeleter(stream, settings[forceLikeDeleteKeyword]);
        await deleter.bulkDelete();
        logger.info('Completed attempt at deleting likes');
      }
      if (bulkType == BulkTypes.all || bulkType == BulkTypes.statuses) {
        logger.info(
            'Attempting to delete all tweets according to ${fromArchive ? "archive" : "Twitter API"}');
        final stream = fromArchive
            ? TwitterArchiveIDStream.fromArchiveTweets(reader)
            : TwitterClientIDStream(
                twitterApi, TwitterClientIDStreamType.statuses);
        final deleter = factory.buildStatusDeleter(stream);
        await deleter.bulkDelete();
        logger.info('Completed attempt at deleting tweets');
      }
      if (bulkType == BulkTypes.all || bulkType == BulkTypes.following) {
        logger.info(
            'Attempting to delete all users you are following according to Twitter API');
        final deleter = factory.buildFollowingDeleter(TwitterClientIDStream(
            twitterApi, TwitterClientIDStreamType.following));
        await deleter.bulkDelete();
        logger.info('Completed attempt at deleting users you follow');
      }
      return;
    }

    final deleteTweetId = settings[deleteTweetKeyword];
    if (deleteTweetId != null) {
      logger.info('Attempting to delete single tweet $deleteTweetId');
      final deleter = factory.buildStatusDeleter(EmptyIdStream());
      await deleter.deleteSpecific(deleteTweetId);
      return;
    }

    final deleteLikeId = settings[deleteLikeKeyword];
    if (deleteLikeId != null) {
      logger.info('Attempting to delete single like $deleteLikeId');
      final deleter = factory.buildLikeDeleter(
          EmptyIdStream(), settings[forceLikeDeleteKeyword]);
      await deleter.deleteSpecific(deleteLikeId);
      return;
    }
  } on Response catch (r) {
    logger.severe(
        'Error response from twitter\nCode: ${r.statusCode}\nReason Phrase: ${r.reasonPhrase}\nReason Body:${r.body}');
  } on TimeoutException catch (t) {
    logger.severe('Timeout error: $t');
  } catch (e) {
    logger.severe('Unknown exception: $e');
  }
}
