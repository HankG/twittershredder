import 'top_id_stream_interface.dart';

class EmptyIdStream implements ITopIDStream {
  @override
  Stream<String> ids() async* {
    yield 'noid';
  }
}
