import 'package:logging/logging.dart';

import '../twitter/twitter_archive_reader.dart';
import 'top_id_stream_interface.dart';

final _logger = Logger('TwitterArchiveIDStream');

class TwitterArchiveIDStream implements ITopIDStream {
  final List<TweetAndLikeData> data;

  TwitterArchiveIDStream(this.data);

  TwitterArchiveIDStream.fromArchiveLikes(TwitterArchiveReader reader)
      : data = reader.getLikes().fold(
            onSuccess: (success) => success,
            onError: (error) {
              _logger.severe(error);
              return [];
            });

  TwitterArchiveIDStream.fromArchiveTweets(TwitterArchiveReader reader)
      : data = reader.getTweets().fold(
            onSuccess: (success) => success,
            onError: (error) {
              _logger.severe(error);
              return [];
            });

  @override
  Stream<String> ids() async* {
    for (final item in data) {
      yield item.id;
    }
  }
}
