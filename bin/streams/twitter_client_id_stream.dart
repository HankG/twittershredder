import 'package:dart_twitter_api/twitter_api.dart';

import 'top_id_stream_interface.dart';

enum TwitterClientIDStreamType {
  following,
  likes,
  statuses,
}

class TwitterClientIDStream implements ITopIDStream {
  final TwitterApi twitterApi;
  final TwitterClientIDStreamType type;
  final _seenIds = <String>{};

  TwitterClientIDStream(this.twitterApi, this.type);

  @override
  Stream<String> ids() async* {
    final list = <String>[];
    list.addAll(await _getMoreIds());
    while (list.isNotEmpty) {
      final id = list.removeAt(0);
      yield id;
      if (list.isEmpty) {
        list.addAll(await _getMoreIds());
      }
    }
  }

  Future<List<String>> _getMoreIds() async {
    final newIds = <String>[];
    switch (type) {
      case TwitterClientIDStreamType.following:
        newIds.addAll(await _getFollowers());
        break;
      case TwitterClientIDStreamType.likes:
        newIds.addAll(await _getLikes());
        break;
      case TwitterClientIDStreamType.statuses:
        newIds.addAll(await _getTweets());
        break;
    }

    final idsToReturn = newIds.where((id) => !_seenIds.contains(id)).toList();
    _seenIds.addAll(idsToReturn);
    return idsToReturn;
  }

  Future<List<String>> _getFollowers() async {
    final topFollowers = await twitterApi.userService.friendsList();
    final ids = topFollowers.users?.map((u) => u.idStr ?? '').toList();
    return ids ?? [];
  }

  Future<List<String>> _getTweets() async {
    final topTweets = await twitterApi.timelineService.userTimeline();
    return topTweets.map((t) => t.idStr ?? '').toList();
  }

  Future<List<String>> _getLikes() async {
    final topLikes = await twitterApi.tweetService.listFavorites();
    return topLikes.map((t) => t.idStr ?? '').toList();
  }
}
