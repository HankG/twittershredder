import 'dart:math';

extension StringUtil on String {
  String truncate({int count = 50}) =>
      "${substring(0, min(count, length))}${(length > count) ? '...' : ''}";
}
