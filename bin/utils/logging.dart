import 'package:logging/logging.dart';

Future<void> setupLogging() async {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((event) {
    print('${event.level.name} @ ${event.time}: ${event.message}');
  });
  return;
}
