import 'package:args/args.dart';

import '../types/bulk_types.dart';

const defaultHistoryPath = './history.db';
const bulkKeyword = 'bulk';
const credentialsKeyword = 'credentials';
const deleteTweetKeyword = 'delete-tweet';
const deleteLikeKeyword = 'delete-like';
const forceLikeDeleteKeyword = 'force-like-delete';
const useArchiveKeyword = 'use-archive';
const historyLocation = 'historydb-path';

ArgParser buildArgs() => ArgParser()
  ..addOption(credentialsKeyword,
      help: 'Twitter credentials file for accessing the API', mandatory: true)
  ..addOption(bulkKeyword,
      help: 'Bulk delete option', allowed: BulkTypes.values.map((e) => e.name))
  ..addOption(deleteTweetKeyword, help: 'Delete specific tweet')
  ..addOption(deleteLikeKeyword, help: 'Delete specific like')
  ..addFlag(forceLikeDeleteKeyword,
      help:
          'Whether to force the deletion of a Like by liking first. Helps with deletion of phantom liked tweets. By default just attempts the delete',
      defaultsTo: false)
  ..addOption(useArchiveKeyword,
      help:
          'Use a specified Twitter archive folder path for bulk Status/Like delete source (otherwise uses API)')
  ..addOption(historyLocation,
      help: 'Specify a history location', defaultsTo: defaultHistoryPath);
