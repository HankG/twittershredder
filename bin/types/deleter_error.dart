enum DeleterErrorType {
  apiError,
  alreadyExists,
  rateLimit,
  other,
}

class DeleterError {
  DeleterErrorType type;
  String message;

  DeleterError({required this.type, this.message = ''});

  @override
  String toString() {
    return 'DeleterError{type: $type, message: $message}';
  }
}
