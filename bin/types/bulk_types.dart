enum BulkTypes {
  all,
  following,
  likes,
  statuses;

  static BulkTypes? parse(String value) => value.isNotEmpty
      ? BulkTypes.values.firstWhere((e) => e.name == value,
          orElse: () => throw Exception("Can't map $value to BulkTypes enum"))
      : null;
}
