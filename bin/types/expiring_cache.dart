import 'package:result_monad/result_monad.dart';

// A really crappy time expiring cache but it'll get the job done for now.
class ExpiringCache<T> {
  final int maxLength;
  final Duration expirationTime;

  final List<_CacheItem<T>> _data = [];

  List<T> get data => List.unmodifiable(_data.map((e) => e.item));

  int get length => _data.length;

  bool get isFull => _data.length == maxLength;

  bool get isNotFull => !isFull;

  bool get isEmpty => _data.isEmpty;

  bool get isNotEmpty => _data.isNotEmpty;

  ExpiringCache(this.maxLength, this.expirationTime);

  Result<T, String> addItem(T item) {
    purge();
    if (isFull) {
      return Result.error('Cache is full w/$maxLength items');
    }
    _data.add(_CacheItem(DateTime.now(), item));
    return Result.ok(item);
  }

  void purge() {
    final expTime = DateTime.now().subtract(expirationTime);
    _data.removeWhere((element) => element.insertionTime.isBefore(expTime));
  }
}

class _CacheItem<T> {
  final DateTime insertionTime;
  final T item;

  _CacheItem(this.insertionTime, this.item);
}
