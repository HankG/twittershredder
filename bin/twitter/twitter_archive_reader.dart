import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart' as p;
import 'package:result_monad/result_monad.dart';

class TwitterArchiveReader {
  final String rootFolder;

  TwitterArchiveReader(this.rootFolder);

  Result<List<TweetAndLikeData>, String> getLikes() {
    try {
      final path = p.join(rootFolder, 'data', 'like.js');
      final json = _getJsonArrayFromTwitterJsFile(path);
      final likes = json.map((j) => TweetAndLikeData(
          id: j['like']['tweetId'], fullText: j['like']['fullText']));
      return Result.ok(likes.toList(growable: false));
    } catch (e) {
      return Result.error(e.toString());
    }
  }

  Result<List<TweetAndLikeData>, String> getTweets() {
    try {
      final path = p.join(rootFolder, 'data', 'tweet.js');
      final json = _getJsonArrayFromTwitterJsFile(path);
      final likes = json.map((j) => TweetAndLikeData(
          id: j['tweet']['id_str'], fullText: j['tweet']['full_text']));
      return Result.ok(likes.toList(growable: false));
    } catch (e) {
      return Result.error(e.toString());
    }
  }

  List<dynamic> _getJsonArrayFromTwitterJsFile(String path) {
    final rawFile = File(path).readAsStringSync();
    final firstEqualsSign = rawFile.indexOf('=');
    final listArrayData = rawFile.substring(firstEqualsSign + 1);
    final json = jsonDecode(listArrayData) as List<dynamic>;
    return json;
  }
}

class TweetAndLikeData {
  final String id;
  final String fullText;

  @override
  String toString() {
    return 'TweetAndLikeData{id: $id, fullText: $fullText}';
  }

  TweetAndLikeData({required this.id, required this.fullText});
}
