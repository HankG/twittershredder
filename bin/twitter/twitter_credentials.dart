import 'dart:convert';
import 'dart:io';

import 'package:dart_twitter_api/twitter_api.dart';
import 'package:result_monad/result_monad.dart';

Result<TwitterClient, String> fromTSettingsJsonFile(String settingsFilePath) {
  try {
    final settings = jsonDecode(File(settingsFilePath).readAsStringSync());
    return Result.ok(TwitterClient(
      consumerKey: settings['consumerKey'],
      consumerSecret: settings['consumerSecret'],
      token: settings['tokenData']['token'],
      secret: settings['tokenData']['secret'],
    ));
  } catch (e) {
    return Result.error(e.toString());
  }
}
