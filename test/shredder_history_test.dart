import 'package:test/test.dart';

import '../bin/history/shredder_history.dart';

void main() {
  test('construction', () {
    final history = ShredderHistory(
        '/tmp/test_${DateTime.now().microsecondsSinceEpoch}.db');
    print(history.historyDbPath);
  });

  test('add item', () {
    final history = ShredderHistory(
        '/tmp/test_${DateTime.now().microsecondsSinceEpoch}.db');
    final result = history.addItem('1', 'success');
    print(result);
    expect(result.isSuccess, true);
  });

  test('add duplicate item fails', () {
    final history = ShredderHistory(
        '/tmp/test_${DateTime.now().microsecondsSinceEpoch}.db');
    final result1 = history.addItem('1', 'success');
    expect(result1.isSuccess, true);
    final result2 = history.addItem('1', 'success');
    print(result2);
    expect(result2.isFailure, true);
  });

  test('retrieve items', () {
    final history = ShredderHistory(
        '/tmp/test_${DateTime.now().microsecondsSinceEpoch}.db');
    history.addItem('1', '200');
    history.addItem('2', '404');
    history.addItem('3', '403');
    final foundResult = history.getItem('2');
    print(foundResult);
    expect(foundResult.isSuccess, true);
    expect(foundResult.value.id, equals('2'));
    expect(foundResult.value.status, equals('404'));
    final notFoundResult = history.getItem('');
    expect(notFoundResult.isFailure, true);
  });

  test('persistence', () {
    final filename = '/tmp/test_${DateTime.now().microsecondsSinceEpoch}.db';
    final history = ShredderHistory(filename);
    history.addItem('1', '200');
    history.addItem('2', '404');
    history.addItem('3', '403');
    history.dispose();

    final reopenedHistory = ShredderHistory(filename);
    final foundResult = reopenedHistory.getItem('2');
    print(foundResult);
    expect(foundResult.isSuccess, true);
  });
}
