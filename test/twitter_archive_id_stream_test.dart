import 'package:test/test.dart';

import '../bin/streams/twitter_archive_id_stream.dart';
import '../bin/twitter/twitter_archive_reader.dart';

const maxCount = 30;
const rootFolder = '/tmp/twitter_archive';

void main() {
  test('simple construction', () async {
    final stream = TwitterArchiveIDStream([
      TweetAndLikeData(id: '1', fullText: '1'),
      TweetAndLikeData(id: '2', fullText: '2')
    ]);
    await for (final id in stream.ids()) {
      print(id);
    }
  });

  test('test likes', () async {
    final stream = TwitterArchiveIDStream.fromArchiveLikes(
        TwitterArchiveReader(rootFolder));
    await for (final id in stream.ids().take(maxCount)) {
      print(id);
    }
  });

  test('test tweets', () async {
    final stream = TwitterArchiveIDStream.fromArchiveTweets(
        TwitterArchiveReader(rootFolder));
    await for (final id in stream.ids().take(maxCount)) {
      print(id);
    }
  });

  test('test likes to empty', () async {
    final stream = TwitterArchiveIDStream.fromArchiveLikes(
        TwitterArchiveReader(rootFolder));
    int count = 0;
    await for (final _ in stream.ids()) {
      count++;
    }
    print('Went through $count likes');
  });

  test('test tweets', () async {
    final stream = TwitterArchiveIDStream.fromArchiveTweets(
        TwitterArchiveReader(rootFolder));
    int count = 0;
    await for (final _ in stream.ids()) {
      count++;
    }
    print('Went through $count tweets');
  });
}
