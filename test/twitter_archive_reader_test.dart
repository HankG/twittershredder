import 'package:test/test.dart';

import '../bin/twitter/twitter_archive_reader.dart';

const rootFolder = '/tmp/twitter_archive';

void main() {
  test('read likes', () {
    final reader = TwitterArchiveReader(rootFolder);
    final likesResult = reader.getLikes();
    expect(likesResult.isSuccess, true);
    print(likesResult.value.first);
  });

  test('read tweets', () {
    final reader = TwitterArchiveReader(rootFolder);
    final tweetsResults = reader.getTweets();
    expect(tweetsResults.isSuccess, true);
    print(tweetsResults.value.first);
  });
}
