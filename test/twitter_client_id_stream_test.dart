import 'package:dart_twitter_api/twitter_api.dart';
import 'package:test/test.dart';

import '../bin/streams/twitter_client_id_stream.dart';
import '../bin/twitter/twitter_credentials.dart';

const maxCount = 10;
const rootFolder = '/tmp/twitter_archive';
const credentialsFile = '/tmp/twitter_credentials.json';
final api = TwitterApi(client: fromTSettingsJsonFile(credentialsFile).value);

void main() {
  test('test likes', () async {
    final stream = TwitterClientIDStream(api, TwitterClientIDStreamType.likes);
    await for (final id in stream.ids().take(maxCount)) {
      print(id);
    }
  });

  test('test tweets', () async {
    final stream =
        TwitterClientIDStream(api, TwitterClientIDStreamType.statuses);
    await for (final id in stream.ids().take(maxCount)) {
      print(id);
    }
  });

  test('test follows', () async {
    final stream =
        TwitterClientIDStream(api, TwitterClientIDStreamType.following);
    await for (final id in stream.ids().take(maxCount)) {
      print(id);
    }
  });
}
