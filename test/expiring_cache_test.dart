import 'dart:io';

import 'package:test/test.dart';

import '../bin/types/expiring_cache.dart';

void main() {
  test('test throughput limiting', () {
    final cache = ExpiringCache<String>(10, Duration(seconds: 1));
    int totalAdds = 0;
    final start = DateTime.now();
    final testElapsed = Duration(seconds: 10);
    while (
        DateTime.now().difference(start).inSeconds <= testElapsed.inSeconds) {
      if (cache.addItem(DateTime.now().toIso8601String()).isSuccess) {
        totalAdds++;
      }
      sleep(Duration(milliseconds: 100));
    }
    final throughput = totalAdds.toDouble() /
        (DateTime.now().difference(start).inMilliseconds.toDouble() / 1000.0);
    print('$throughput adds/second');
    expect(cache.isFull, true);
  });

  test('test decay to zero', () {
    final cache = ExpiringCache<int>(20, Duration(seconds: 10));
    List.generate(10, (index) {
      cache.addItem(10);
      sleep(Duration(seconds: 1));
    });

    final start = DateTime.now();
    final testElapsed = Duration(minutes: 1);
    while (
        DateTime.now().difference(start).inSeconds <= testElapsed.inSeconds &&
            cache.isNotEmpty) {
      cache.purge();
      sleep(Duration(milliseconds: 100));
    }
    expect(cache.isEmpty, true);
  });
}
