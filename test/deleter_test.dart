import 'package:http/http.dart';
import 'package:result_monad/src/result_monad_base.dart';
import 'package:test/test.dart';

import '../bin/history/history_interface.dart';
import '../bin/history/shredder_history.dart';
import '../bin/ops/deleter.dart';
import '../bin/streams/top_id_stream_interface.dart';
import '../bin/types/expiring_cache.dart';

void main() {
  late Deleter<String> deleter;
  late List<String> ids;

  setUp(() {
    const maxLength = 10;
    final duration = Duration(seconds: 20);
    ids = List.generate(100, (index) => '$index');
    deleter = Deleter(
        idStream: _SampleIdStream(ids),
        history: _SampleHistory(),
        rateLimitCache: ExpiringCache<String>(maxLength, duration),
        deleteFunction: (id) async =>
            ids.remove(id) ? id : throw Response('Value $id not found', 404),
        statusFunction: (id) => print('Deleted $id'),
        unitType: 'ID');
  });

  test('test delete individual', () async {
    final idToDelete = ids[1];
    final result = await deleter.deleteSpecific(idToDelete);
    print(result);
    expect(result.isSuccess, true);
    expect(result.value, equals(idToDelete));
    expect(ids.contains(idToDelete), false);
  });

  test('test delete bulk w/max', () async {
    const max = 10;
    final result = await deleter.bulkDelete(
        max: max, normalSleep: Duration(milliseconds: 1));
    expect(result.isSuccess, true);
    expect(result.value, equals(max));
    expect(ids.length, equals(90));
    expect(ids.first, equals('10'));
  });

  test('test delete bulk rate limiting', () async {
    const max = 20;
    final result = await deleter.bulkDelete(
        max: max,
        normalSleep: Duration(milliseconds: 1),
        rateLimitSleep: Duration(seconds: 1),
        longSleep: Duration(seconds: 5));
    expect(result.isSuccess, true);
    expect(result.value, equals(max));
    expect(ids.length, equals(80));
    expect(ids.first, equals('20'));
  });

  test('test delete unknown', () async {
    final idToDelete = 'hello';
    final result = await deleter.deleteSpecific(idToDelete);
    print(result);
    expect(result.isFailure, true);
  });
}

class _SampleHistory implements IHistory {
  final _data = <String, HistoryItem>{};

  @override
  Result<HistoryItem, String> addItem(String id, String status) {
    if (_data.containsKey(id)) {
      return Result.error('Duplicate $id');
    }

    _data[id] = HistoryItem(id: id, timestamp: DateTime.now(), status: status);
    return Result.ok(_data[id]!);
  }

  @override
  Result<HistoryItem, String> getItem(String id) {
    if (!_data.containsKey(id)) {
      return Result.error("$id not found");
    }

    return Result.ok(_data[id]!);
  }
}

class _SampleIdStream implements ITopIDStream {
  final List<String> data;

  _SampleIdStream(List<String> originalSet)
      : data = List.unmodifiable(originalSet);

  @override
  Stream<String> ids() async* {
    for (final id in data) {
      yield id;
    }
  }
}
